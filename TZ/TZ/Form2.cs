﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZ
{
    public partial class Form2 : Form
    {
        private void button1_Click(object sender, EventArgs e)
        {
           var emp = TextAdd();
           if (emp == null) { MessageBox.Show("Некорректные данные!"); return; }
           if( DataManager.Insert(emp)) MessageBox.Show("Данные добавлены!");
        }

        public Form2()
        {
            InitializeComponent();
        }

        public Employee TextAdd()
        {
            string date = textBox3.Text;
            if (!DateTime.TryParse(date, out DateTime dateTime)) return null;


            Employee employee = new Employee();

            if (radioButton2.Checked) employee.type = EmployeeType.Manager;
            else if (radioButton1.Checked) employee.type = EmployeeType.Employee;
            else employee.type = EmployeeType.Salesman;
            employee.FirstName = textBox1.Text;
            employee.LastName = textBox2.Text;
            employee.Date = textBox3.Text;
            employee.money = textBox4.Text;
            return employee;
        }

 
    }
}
