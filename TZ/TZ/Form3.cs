﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZ
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            LoadData();
        }
        public string[] data = new string[3];
        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            string[] info = name.Split(' ');
            if (info.Length != 2) { MessageBox.Show("Введите имя и фамилию!"); return; }
            Employee employee =  DataManager.FindByName(info[0],info[1]);
            if(employee==null) { MessageBox.Show("Нет совпадений!"); return; }
            float salary = DataManager.Calculate(employee, DateTime.Now);
            MessageBox.Show(salary.ToString());


        }

        
        private void LoadData()
        {
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=TestDB.db; Version=3;"))
            {
                Connect.Open(); // открыть соединение
                SQLiteCommand Command = new SQLiteCommand
                {
                    Connection = Connect,

                    CommandText = $"SELECT * FROM Customers;"
                };
                SQLiteDataReader sqlReader = Command.ExecuteReader();           
                List<string[]> data = new List<string[]>();
                while (sqlReader.Read())
                {
                    data.Add(new string[6]);

                    data[data.Count - 1][0] = sqlReader[0].ToString();
                    data[data.Count - 1][1] = sqlReader[1].ToString();
                    data[data.Count - 1][2] = sqlReader[2].ToString();
                    data[data.Count - 1][3] = sqlReader[3].ToString();
                    data[data.Count - 1][4] = sqlReader[4].ToString();
                    data[data.Count - 1][5] = sqlReader[5].ToString();
                   
                }
                sqlReader.Close();
                Connect.Close();
                foreach (string[] s in data)
                    dataGridView1.Rows.Add(s);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(DataManager.CalculateAll().ToString());   
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string date = textBox2.Text;
            string name = textBox1.Text;
            string[] info = name.Split(' ');
            if (info.Length != 2) { MessageBox.Show("Введите имя и фамилию!"); return; }
            Employee employee = DataManager.FindByName(info[0], info[1]);
            if (employee == null) { MessageBox.Show("Нет совпадений!"); return; }
            if (!DateTime.TryParse(date, out DateTime dateTime)){ MessageBox.Show("Некорректные данные!"); return; }
            if (dateTime < employee.GetDate()) { MessageBox.Show("Некорректные данные!"); return; }
            MessageBox.Show(DataManager.Calculate(employee, dateTime).ToString());
        }
    }
}


