﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
namespace TZ
{
    public static class DataManager
    {
        public static List<Employee> employees = new List<Employee>();
        public static void Init()
        {
            if (!File.Exists(@"TestDB.db"))
            {
                SQLiteConnection.CreateFile(@"TestDB.db");
            }
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=TestDB.db; Version=3;"))
            {
                // строка запроса, который надо будет выполнить
                string commandText = "CREATE TABLE IF NOT EXISTS Customers (Id INTEGER PRIMARY KEY AUTOINCREMENT,FirstName VARCHAR(20),LastName VARCHAR(20),Type VARCHAR(30),date VARCHAR(15),zp DECIMAL)"; // создать таблицу, если её нет
                SQLiteCommand Command = new SQLiteCommand(commandText, Connect);
                Connect.Open(); // открыть соединение
                Command.ExecuteNonQuery(); // выполнить запрос
                Connect.Close(); // закрыть соединение
            }

        }

        public static float Calculate(Employee employee, DateTime time)
        {
            float koef = 0;
            float money = float.Parse(employee.money);
            float maxBonus = 1;
            switch (employee.type)
            {
                case EmployeeType.Employee:
                    koef = 0.03f;
                    maxBonus = 0.3f;
                    break;
                case EmployeeType.Manager:
                    koef = 0.05f;
                    maxBonus = 0.4f;
                    break;
                case EmployeeType.Salesman:
                    koef = 0.01f;
                    maxBonus = 0.35f;
                    break;
            }
            int year = CalculateAgeCorrect(employee.GetDate(), time);
            float bonus = koef * year;
            if (bonus > maxBonus) bonus = maxBonus;
            float salary = money + (money * bonus);
            return salary;
        }
        public static Employee FindByName(string name, string Lastname)
        {
            foreach(Employee employee in employees)
            {
                if (employee.FirstName == name && employee.LastName == Lastname) return employee;
                if (employee.FirstName == Lastname && employee.LastName == name) return employee;
            }
            return null;
        }

        public static float CalculateAll()
        {
            float salaryAll = 0;
            foreach (Employee employee in employees)
            {
                salaryAll += Calculate(employee, DateTime.Now);
            }
            return salaryAll;
        }
        public static int CalculateAgeCorrect(DateTime ArDate, DateTime now)
        {
            int Experience = now.Year - ArDate.Year;

            if (now.Month < ArDate.Month || (now.Month == ArDate.Month && now.Day < ArDate.Day))
                Experience--;

            return Experience;
        }

        public static void Load()
        {
            if (!File.Exists(@"TestDB.db")) return;
            using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=TestDB.db; Version=3;"))
            {
                Connect.Open();
                SQLiteCommand Command = new SQLiteCommand
                {
                    Connection = Connect,
                    CommandText = @"SELECT * FROM Customers"
                };

                SQLiteDataReader sqlReader = Command.ExecuteReader();
                int count = 0;
                List<string> firstNames = new List<string>();
                List<string> lastNames = new List<string>();
                List<EmployeeType> Types = new List<EmployeeType>();
                List<string> date = new List<string>();
                List<string> zp = new List<string>();
                while (sqlReader.Read()) // считываем и вносим в лист все параметры
                {
                    string en = sqlReader["Type"].ToString();
                    switch (en)
                    {
                        case "Employee":
                            Types.Add(EmployeeType.Employee);
                            break;
                        case "Manager":
                            Types.Add(EmployeeType.Manager);
                            break;
                        case "Salesman":
                            Types.Add(EmployeeType.Salesman);
                            break;
                        default: continue;
                    }

                    firstNames.Add(sqlReader["FirstName"].ToString());
                    lastNames.Add(sqlReader["LastName"].ToString());
                    date.Add(sqlReader["date"].ToString());
                    zp.Add(sqlReader["zp"].ToString());
                    count++;
                }
                for (int i = 0; i < count; i++)
                {
                    Employee employee = new Employee();
                    employee.FirstName = firstNames[i];
                    employee.LastName = lastNames[i];
                    employee.type = Types[i];
                    employee.Date = date[i];
                    employee.money = zp[i];
                    employees.Add(employee);
                }
                Connect.Close();
            }

        }

        public static bool Insert(Employee employee)
        {
            try
            {
                using (SQLiteConnection Connect = new SQLiteConnection(@"Data Source=TestDB.db; Version=3;"))
                {
                    string FirstName = employee.FirstName;
                    string LastName = employee.LastName;
                    string type = employee.type.ToString();
                    string date = employee.Date;
                    string zp = employee.money;
                    string commandText = "INSERT INTO Customers (FirstName,LastName,Type,date,zp) VALUES(@FirstName,@LastName,@Type,@date,@zp)";
                    SQLiteCommand Command = new SQLiteCommand(commandText, Connect);
                    Command.Parameters.AddWithValue("@FirstName", FirstName);
                    Command.Parameters.AddWithValue("@LastName",LastName);
                    Command.Parameters.AddWithValue("@Type", type);
                    Command.Parameters.AddWithValue("@date", date);
                    Command.Parameters.AddWithValue("@zp", zp);
                    Connect.Open(); // открыть соединение
                    Command.ExecuteNonQuery(); // выполнить запрос
                    Connect.Close(); // закрыть соединение
                }
                employees.Add(employee);
                return true; 
            } 
            catch { return false; }
           
           
        }



    }
}