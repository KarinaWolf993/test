﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TZ
{
    public partial class Form1 : Form
    {
        Form2 form2 = new Form2();
        Form3 form3 = new Form3();
        public Form1()
        {
            InitializeComponent();
            DataManager.Init();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (form2 == null || form2.IsDisposed) form2 = new Form2();
            form2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (form3 == null || form3.IsDisposed) form3 = new Form3();
            form3.Show();
        }
    }
}
