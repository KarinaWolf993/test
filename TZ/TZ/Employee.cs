﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZ
{
    public class Employee
    {
        public string FirstName, LastName, Date, money;
        public EmployeeType type;
        public DateTime GetDate()
        {
            DateTime data = DateTime.Parse(Date);
            return data;
        }
    }

   public enum EmployeeType
    {
        Employee,
        Manager,
        Salesman
    } 

}
